document.addEventListener("DOMContentLoaded", () => {
  const content = document.querySelector("#content")
  content.addEventListener("click", (event) => {
    const target = event.target
    if (window.innerWidth < 800 && !target.closest("a")) {
      const about = document.querySelector("#about")
      const logo = document.querySelector("#logo")

      about.classList.toggle("visible")
      logo.classList.toggle("unvisible")
    }
  })
})
